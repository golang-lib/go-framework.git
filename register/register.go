package register

type Config struct {
	RegSrv          string `json:"regSrv"`          // 注册服务器的地址
	Host            string `json:"host,omitempty"`  // 本服务主机名称
	Port            int    `json:"port"`            // 本服务端口
	Token           string `json:"token"`           // acl token
	DeRegisterAfter string `json:"deRegisterAfter"` // 健康检查失败后多长时间实例被移除
}

type Registration struct {
	ServiceID       string
	ServiceName     string
	ServiceHost     string
	ServicePort     int
	Tags            []string
	DeRegisterAfter string // 健康检查失败后多长时间实例被移除
}

type ServiceRegister interface {
	RegisterService(reg *Registration) error
}
