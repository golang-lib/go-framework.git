package zlog

import (
	"go.uber.org/zap"
	"testing"
)

type User struct {
	Name string
}

func Test1(t *testing.T) {
	InitZLog("./zlog.log")

	user := &User{
		Name: "bolo",
	}

	Info("test log", zap.Any("user", user))
}
