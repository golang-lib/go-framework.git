package filesystem

import (
	"os"
	"testing"
)

func TestToChmodPerm(t *testing.T) {
	tests := []struct {
		mode     os.FileMode
		wantPerm uint32
	}{
		{
			os.FileMode(0000),
			0000,
		},
		{
			os.FileMode(0777),
			0777,
		},
		{
			os.FileMode(0660),
			0660,
		},
		{
			os.FileMode(0755),
			0755,
		},
	}
	for _, tt := range tests {
		t.Run("", func(t *testing.T) {
			if gotPerm := ToChmodPerm(tt.mode); gotPerm != tt.wantPerm {
				t.Errorf("ToChmodPerm() = %v, want %v", gotPerm, tt.wantPerm)
			}
		})
	}
}
