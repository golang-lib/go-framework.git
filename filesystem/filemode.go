package filesystem

import (
	"os"
	"syscall"
)

const (
	sIsuid = syscall.S_ISUID
	sIsgid = syscall.S_ISGID
	sIsvtx = syscall.S_ISVTX
)

// ToChmodPerm converts Go permission bits to POSIX permission bits.
//
// This differs from fromFileMode in that we preserve the POSIX versions of
// setuid, setgid and sticky in m, because we've historically supported those
// bits, and we mask off any non-permission bits.
func ToChmodPerm(m os.FileMode) (perm uint32) {
	const mask = os.ModePerm | sIsuid | sIsgid | sIsvtx
	perm = uint32(m & mask)

	if m&os.ModeSetuid != 0 {
		perm |= sIsuid
	}
	if m&os.ModeSetgid != 0 {
		perm |= sIsgid
	}
	if m&os.ModeSticky != 0 {
		perm |= sIsvtx
	}

	return perm
}
