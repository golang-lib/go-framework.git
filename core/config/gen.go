package config

type Gen struct {
	Dbname    string `mapstructure:"dbname" json:"dbname" yaml:"dbname"`
	Frontpath string `mapstructure:"frontpath" json:"frontpath" yaml:"frontpath"`
}
