package web_test

import (
	"fmt"
	"gitee.com/golang-lib/go-framework/core/web"
	"gitee.com/golang-lib/go-framework/stl/util"
	"net/http"
	"testing"
)

func TestFuncFilter(t *testing.T) {

	funcFilter := web.FuncFilter(func(ctx web.Context, chain web.FilterChain) {
		fmt.Println("@FuncFilter")
		chain.Next(ctx)
	})

	handlerFilter := web.HandlerFilter(web.FUNC(func(ctx web.Context) {
		fmt.Println("@HandlerFilter")
	}))

	web.NewDefaultFilterChain([]web.Filter{funcFilter, handlerFilter}).Next(nil)
}

type Counter struct{}

func (ctr *Counter) ServeHTTP(http.ResponseWriter, *http.Request) {}

func TestWrapH(t *testing.T) {

	c := &Counter{}
	fmt.Println(util.FileLine(c.ServeHTTP))

	var h http.Handler
	h = &Counter{}
	fmt.Println(util.FileLine(h.ServeHTTP))

	fmt.Println(web.WrapH(&Counter{}).FileLine())
}
