package web_test

/*
func TestRpcError(t *testing.T) {
	err := errors.New("this is an error")

	r1 := web.ERROR.Error(err)
	assert.Equal(t, r1, &web.RpcResult{
		ErrorCode: web.ErrorCode(web.ERROR),
		Err:       "/Users/didi/GitHub/go-spring/go-spring/spring/spring-core/web/rpc-result_test.go:30: this is an error",
	})

	r2 := web.ERROR.ErrorWithData(err, "error_with_data")
	assert.Equal(t, r2, &web.RpcResult{
		ErrorCode: web.ErrorCode(web.ERROR),
		Err:       "/Users/didi/GitHub/go-spring/go-spring/spring/spring-core/web/rpc-result_test.go:36: this is an error",
		Data:      "error_with_data",
	})

	func() {
		defer func() {
			assert.Equal(t, recover(), &web.RpcResult{
				ErrorCode: web.ErrorCode(web.ERROR),
				Err:       "/Users/didi/GitHub/go-spring/go-spring/spring/spring-core/web/rpc-result_test.go:50: this is an error",
			})
		}()
		web.ERROR.Panic(err).When(err != nil)
	}()

	func() {
		defer func() {
			assert.Equal(t, recover(), &web.RpcResult{
				ErrorCode: web.ErrorCode(web.ERROR),
				Err:       "/Users/didi/GitHub/go-spring/go-spring/spring/spring-core/web/rpc-result_test.go:60: this is an error",
			})
		}()
		web.ERROR.Panicf(err.Error()).When(true)
	}()

	func() {
		defer func() {
			assert.Equal(t, recover(), &web.RpcResult{
				ErrorCode: web.ErrorCode(web.ERROR),
				Err:       "/Users/didi/GitHub/go-spring/go-spring/spring/spring-core/web/rpc-result_test.go:70: this is an error",
			})
		}()
		web.ERROR.PanicImmediately(err)
	}()
}
*/
