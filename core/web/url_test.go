package web_test

import (
	"gitee.com/golang-lib/go-framework/core/web"
	"gitee.com/golang-lib/go-framework/stl/assert"
	"testing"
)

func TestToPathStyle(t *testing.T) {

	t.Run("/:a", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/:a", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/:a", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/:a", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}")
		assert.Equal(t, wildCardName, "")
	})

	t.Run("/{a}", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/{a}", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/{a}", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/{a}", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}")
		assert.Equal(t, wildCardName, "")
	})

	t.Run("/:a/b/:c", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/:a/b/:c", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/:a/b/:c", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/:a/b/:c", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}/b/{c}")
		assert.Equal(t, wildCardName, "")
	})

	t.Run("/{a}/b/{c}", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/{a}/b/{c}", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}/b/{c}")
		assert.Equal(t, wildCardName, "")
	})

	t.Run("/:a/b/:c/*", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/:a/b/:c/*", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/:a/b/:c/*", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*@_@")
		assert.Equal(t, wildCardName, "@_@")
		newPath, wildCardName = web.ToPathStyle("/:a/b/:c/*", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}/b/{c}/{*}")
		assert.Equal(t, wildCardName, "")
	})

	t.Run("/{a}/b/{c}/{*}", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/{a}/b/{c}/{*}", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*")
		assert.Equal(t, wildCardName, "")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}/{*}", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*@_@")
		assert.Equal(t, wildCardName, "@_@")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}/{*}", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}/b/{c}/{*}")
		assert.Equal(t, wildCardName, "")
	})

	t.Run("/:a/b/:c/*e", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/:a/b/:c/*e", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*")
		assert.Equal(t, wildCardName, "e")
		newPath, wildCardName = web.ToPathStyle("/:a/b/:c/*e", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*e")
		assert.Equal(t, wildCardName, "e")
		newPath, wildCardName = web.ToPathStyle("/:a/b/:c/*e", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}/b/{c}/{*:e}")
		assert.Equal(t, wildCardName, "e")
	})

	t.Run("/{a}/b/{c}/{*:e}", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/{a}/b/{c}/{*:e}", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*")
		assert.Equal(t, wildCardName, "e")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}/{*:e}", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*e")
		assert.Equal(t, wildCardName, "e")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}/{*:e}", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}/b/{c}/{*:e}")
		assert.Equal(t, wildCardName, "e")
	})

	t.Run("/{a}/b/{c}/{e:*}", func(t *testing.T) {
		newPath, wildCardName := web.ToPathStyle("/{a}/b/{c}/{e:*}", web.EchoPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*")
		assert.Equal(t, wildCardName, "e")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}/{e:*}", web.GinPathStyle)
		assert.Equal(t, newPath, "/:a/b/:c/*e")
		assert.Equal(t, wildCardName, "e")
		newPath, wildCardName = web.ToPathStyle("/{a}/b/{c}/{e:*}", web.JavaPathStyle)
		assert.Equal(t, newPath, "/{a}/b/{c}/{*:e}")
		assert.Equal(t, wildCardName, "e")
	})
}
