package biz

var (
	Success       = NewBizErrCode(200, "success")
	ClientErr     = NewBizErrCode(400, "client error")
	ServerError   = NewBizErrCode(500, "服务器异常，请联系管理员")
	PermissionErr = NewBizErrCode(4001, "没有权限操作，可能是TOKEN过期了，请先登录")
	CasbinErr     = NewBizErrCode(403, "没有API接口访问权限，请联系管理员")
)

// 业务错误
type CustomError struct {
	code int
	err  string
}

// 错误消息
func (e *CustomError) Error() string {
	return e.err
}

// 错误码
func (e *CustomError) Code() int {
	return e.code
}

// 创建业务逻辑错误结构体，默认为业务逻辑错误
func NewBizErr(msg string) *CustomError {
	return &CustomError{code: ClientErr.code, err: msg}
}

// 创建业务逻辑错误结构体，可设置指定错误code
func NewBizErrCode(code int, msg string) *CustomError {
	return &CustomError{code: code, err: msg}
}
