// Package mq 提供了标准的消息队列接口，可以灵活适配各种 MQ 实现。
package mq

type Message interface {
	Topic() string
	ID() string
	Body() []byte
	Extra() map[string]string
}

type message struct {
	topic string            // 消息主题
	id    string            // Key
	body  []byte            // Value
	extra map[string]string // 额外信息
}

// NewMessage 创建新的消息对象。
func NewMessage() *message {
	return &message{}
}

// Topic 返回消息的主题。
func (msg *message) Topic() string {
	return msg.topic
}

// WithTopic 设置消息的主题。
func (msg *message) WithTopic(topic string) *message {
	msg.topic = topic
	return msg
}

// ID 返回消息的序号。
func (msg *message) ID() string {
	return msg.id
}

// WithID 设置消息的序号。
func (msg *message) WithID(id string) *message {
	msg.id = id
	return msg
}

// Body 返回消息的内容。
func (msg *message) Body() []byte {
	return msg.body
}

// WithBody 设置消息的内容。
func (msg *message) WithBody(body []byte) *message {
	msg.body = body
	return msg
}

// Extra 返回消息的额外信息。
func (msg *message) Extra() map[string]string {
	return msg.extra
}

// WithExtra 为消息添加额外的信息。
func (msg *message) WithExtra(key, value string) *message {
	if msg.extra == nil {
		msg.extra = make(map[string]string)
	}
	msg.extra[key] = value
	return msg
}
