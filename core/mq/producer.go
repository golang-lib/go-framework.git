package mq

import "context"

// Producer 消息生产者。
type Producer interface {

	// SendMessage 传入的消息类型不同用途也不同，需要分别判断。
	SendMessage(ctx context.Context, msg Message) error

}
