package mq

import (
	"context"
	"errors"
	"gitee.com/golang-lib/go-framework/stl/json"
	"gitee.com/golang-lib/go-framework/stl/util"
	"reflect"
)

// Consumer 消息消费者。
type Consumer interface {
	Topics() []string
	Consume(ctx context.Context, msg Message) error
}

// consumer Bind 方式的消息消费者。
type consumer struct {

	// 消息主题列表。
	topics []string

	fn interface{}
	t  reflect.Type
	v  reflect.Value
	e  reflect.Type
}

func (c *consumer) Topics() []string {
	return c.topics
}

func (c *consumer) Consume(ctx context.Context, msg Message) error {
	e := reflect.New(c.e.Elem())
	err := json.Unmarshal(msg.Body(), e.Interface())
	if err != nil {
		return err
	}
	out := c.v.Call([]reflect.Value{reflect.ValueOf(ctx), e})
	if err = out[0].Interface().(error); err != nil {
		return err
	}
	return nil
}

func validBindFn(t reflect.Type) bool {
	return util.IsFuncType(t) &&
		util.ReturnOnlyError(t) &&
		t.NumIn() == 2 &&
		util.IsContextType(t.In(0)) &&
		util.IsStructPtr(t.In(1))
}

// Bind 创建 Bind 方式的消费者。
func Bind(fn interface{}, topics ...string) *consumer {
	if t := reflect.TypeOf(fn); validBindFn(t) {
		return &consumer{
			topics: topics,
			fn:     fn,
			t:      t,
			v:      reflect.ValueOf(fn),
			e:      t.In(1),
		}
	}
	panic(errors.New("fn should be func(ctx,*struct)error"))
}
