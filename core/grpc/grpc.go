package grpc

type Server struct {
	Register interface{} // 服务注册函数
	Service  interface{} // 服务提供者
}
