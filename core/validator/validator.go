package validator

import "reflect"

// Validator 参数校验器接口。
type Validator interface {
	Validate(i interface{}) error
}

var v Validator

// Init 初始化参数校验器。
func Init(r Validator) {
	v = r
}

// InitFunc 初始化参数校验器。
func InitFunc(r func(i interface{}) error) {
	v = funcValidator(r)
}

// funcValidator 基于简单函数的参数校验器。
type funcValidator func(i interface{}) error

func (f funcValidator) Validate(i interface{}) error {
	return f(i)
}

// Validate 参数校验。
func Validate(i interface{}) error {
	if v != nil {
		return v.Validate(i)
	}
	return nil
}

// 判断变量是否为空
func Empty(val interface{}) bool {
	if val == nil {
		return true
	}

	v := reflect.ValueOf(val)
	switch v.Kind() {
	case reflect.String, reflect.Array:
		return v.Len() == 0
	case reflect.Map, reflect.Slice:
		return v.Len() == 0 || v.IsNil()
	case reflect.Bool:
		return !v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return v.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return v.Float() == 0
	case reflect.Interface, reflect.Ptr:
		return v.IsNil()
	}

	return reflect.DeepEqual(val, reflect.Zero(v.Type()).Interface())
}
