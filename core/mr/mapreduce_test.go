package mr

import (
	"testing"
	"time"
)

func Test1(t *testing.T) {
	ch := make(chan interface{})
	defer func() {
		close(ch)
	}()

	go func() {
		// v,ok模式 读取channel
		for {
			v, ok := <-ch
			if !ok {
				return
			}
			t.Log(v)
		}

		// for range模式读取channel，channel关闭循环自动退出
		for i := range ch {
			t.Log(i)
		}

		// 清空channel，channel关闭循环自动退出
		for range ch {
		}
	}()

	for i := 0; i < 10; i++ {
		ch <- i
		time.Sleep(time.Second)
	}
}

func Test2(t *testing.T) {

}
