package http

import (
	"encoding/json"
	"errors"
	"net/http"
)

type ResponseWrapper struct {
	StatusCode int
	Body       []byte
	Header     http.Header
}

func (r *ResponseWrapper) IsSuccess() bool {
	return r.StatusCode == 200
}

func (r *ResponseWrapper) BodyToObj(objPtr interface{}) error {
	_ = json.Unmarshal(r.Body, &objPtr)
	return r.getError()
}

func (r *ResponseWrapper) BodyToString() (string, error) {
	return string(r.Body), r.getError()
}

func (r *ResponseWrapper) BodyToMap() (map[string]interface{}, error) {
	var res map[string]interface{}
	err := json.Unmarshal(r.Body, &res)
	if err != nil {
		return nil, err
	}
	return res, r.getError()
}

func (r *ResponseWrapper) getError() error {
	if !r.IsSuccess() {
		return errors.New(string(r.Body))
	}
	return nil
}
