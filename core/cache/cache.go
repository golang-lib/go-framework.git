package cache

import (
	"runtime"
	"sync"
	"time"
)

type ICache interface {

	// 添加缓存，如果缓存则返回错误
	Add(k string, v interface{}) error

	// 如果不存在则添加缓存值，否则直接返回
	AddIfAbsent(k string, v interface{})

	// 如果存在则直接返回，否则调用getValue回调函数获取值并添加该缓存值
	// @return 缓存值
	ComputeIfAbsent(k string, getValueFunc func(string) (interface{}, error)) (interface{}, error)

	// 获取缓存值，参数1为值，参数2->是否存在该缓存
	Get(k string) (interface{}, bool)

	// 缓存数量
	Count() int

	// 删除缓存
	Delete(k string)

	// 清空所有缓存
	Clear()
}

type Cache struct {
	*cache
}

type cache struct {
	mapping sync.Map
	janitor *janitor
}

type janitor struct {
	interval time.Duration
	stop     chan struct{}
}

type element struct {
	Expired time.Time
	Payload interface{}
}

// Put element in Cache with its ttl
func (c *cache) Put(key interface{}, payload interface{}, ttl time.Duration) {
	c.mapping.Store(key, &element{
		Payload: payload,
		Expired: time.Now().Add(ttl),
	})
}

// Get element in Cache, and drop when it expired
func (c *cache) Get(key interface{}) interface{} {
	item, exist := c.mapping.Load(key)
	if !exist {
		return nil
	}
	elm := item.(*element)
	// expired
	if time.Since(elm.Expired) > 0 {
		c.mapping.Delete(key)
		return nil
	}
	return elm.Payload
}

// GetWithExpire element in Cache with Expire Time
func (c *cache) GetWithExpire(key interface{}) (payload interface{}, expired time.Time) {
	item, exist := c.mapping.Load(key)
	if !exist {
		return
	}
	elm := item.(*element)
	// expired
	if time.Since(elm.Expired) > 0 {
		c.mapping.Delete(key)
		return
	}
	return elm.Payload, elm.Expired
}

func (c *cache) cleanup() {
	c.mapping.Range(func(k, v interface{}) bool {
		key := k.(string)
		elm := v.(*element)
		if time.Since(elm.Expired) > 0 {
			c.mapping.Delete(key)
		}
		return true
	})
}

func (j *janitor) process(c *cache) {
	ticker := time.NewTicker(j.interval)
	for {
		select {
		case <-ticker.C: //从定时器中获取数据
			c.cleanup()
		case <-j.stop:
			ticker.Stop()
			return
		}
	}
}

func stopJanitor(c *Cache) {
	c.janitor.stop <- struct{}{}
}

// New return *Cache
func New(interval time.Duration) *Cache {
	j := &janitor{
		interval: interval,
		stop:     make(chan struct{}),
	}
	c := &cache{janitor: j}
	go j.process(c)
	C := &Cache{c}
	//SetFinalizer只有在对象object被GC时，才会被执行。其他情况下，都不会被执行，即使程序正常结束或者发生错误。
	runtime.SetFinalizer(C, stopJanitor)
	return C
}
