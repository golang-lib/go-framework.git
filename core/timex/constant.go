package timex

import "time"

const (
	timezone   = "Asia/Shanghai"
	TimeFormat = "2006-01-02 15:04:05"
	CSTLayout  = "2006/01/02 15:04:05"
)

var (
	cst *time.Location
)
