package prop

import "github.com/magiconair/properties"

// Read 将 properties 格式的字节数组解析成 map 数据。
func Read(b []byte) (map[string]interface{}, error) {

	p := properties.NewProperties()
	p.DisableExpansion = true

	err := p.Load(b, properties.UTF8)
	if err != nil {
		return nil, err
	}

	ret := make(map[string]interface{})
	for k, v := range p.Map() {
		ret[k] = v
	}
	return ret, nil
}
