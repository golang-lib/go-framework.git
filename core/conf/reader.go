package conf

import (
	"gitee.com/golang-lib/go-framework/core/conf/prop"
	"gitee.com/golang-lib/go-framework/core/conf/toml"
	"gitee.com/golang-lib/go-framework/core/conf/yaml"
)

func init() {
	NewReader(yaml.Read, ".yaml", ".yml")
	NewReader(prop.Read, ".properties")
	NewReader(toml.Read, ".toml")
}

var readers = make(map[string]Reader)

// Reader 属性列表解析器，将字节数组解析成 map 数据。
type Reader func(b []byte) (map[string]interface{}, error)

// NewReader 注册属性列表解析器，ext 是解析器支持的文件扩展名。
func NewReader(r Reader, ext ...string) {
	for _, s := range ext {
		readers[s] = r
	}
}
