package gs_test

import (
	"errors"
	"fmt"
	"gitee.com/golang-lib/go-framework/core/conf"
	"gitee.com/golang-lib/go-framework/core/gs"
	"gitee.com/golang-lib/go-framework/core/gs/environ"
	"gitee.com/golang-lib/go-framework/stl/assert"
	"os"
	"sort"
	"testing"
	"time"
)

func startApplication(cfgLocation string) (*gs.App, gs.Pandora) {

	app := gs.NewApp()
	gs.Setenv("SPRING_BANNER_VISIBLE", true)
	gs.Setenv("SPRING_CONFIG_LOCATION", cfgLocation)
	app.Property(environ.EnablePandora, true)

	var p gs.Pandora
	type PandoraAware struct{}
	app.Provide(func(b gs.Pandora) PandoraAware {
		p = b
		return PandoraAware{}
	})

	go app.Run()
	time.Sleep(100 * time.Millisecond)
	return app, p
}

func TestConfig(t *testing.T) {

	t.Run("config via env", func(t *testing.T) {
		os.Clearenv()
		gs.Setenv("GS_SPRING_PROFILES_ACTIVE", "dev")
		app, p := startApplication("testdata/config/")
		defer app.ShutDown(errors.New("run test end"))
		assert.Equal(t, p.Prop(environ.SpringProfilesActive), "dev")
	})

	t.Run("config via env 2", func(t *testing.T) {
		os.Clearenv()
		gs.Setenv("GS_SPRING_PROFILES_ACTIVE", "dev")
		app, p := startApplication("testdata/config/")
		defer app.ShutDown(errors.New("run test end"))
		assert.Equal(t, p.Prop(environ.SpringProfilesActive), "dev")
	})

	t.Run("profile via env&config 2", func(t *testing.T) {

		os.Clearenv()
		gs.Setenv("GS_SPRING_PROFILES_ACTIVE", "dev")
		app, p := startApplication("testdata/config/")
		defer app.ShutDown(errors.New("run test end"))
		assert.Equal(t, p.Prop(environ.SpringProfilesActive), "dev")

		var m map[string]string
		_ = p.Bind(&m, conf.Key(conf.RootKey))
		for _, k := range sortedKeys(m) {
			fmt.Println(k, "=", p.Prop(k))
		}
	})
}

func sortedKeys(m map[string]string) (keys []string) {
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return
}
