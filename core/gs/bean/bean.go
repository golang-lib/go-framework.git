// Package bean 定义了 bean 选择器和 Definition 接口。
package bean

import "reflect"

// Definition bean 元数据。
type Definition interface {
	Type() reflect.Type     // 类型
	Value() reflect.Value   // 值
	Interface() interface{} // 源
	ID() string             // 返回 bean 的 ID
	BeanName() string       // 返回 bean 的名称
	TypeName() string       // 返回类型的全限定名
	Wired() bool            // 返回是否已完成注入
}

// Selector bean 选择器，可以是 bean ID 字符串，可以是 reflect.Type 对
// 象，可以是形如 (*error)(nil) 的指针，还可以是 Definition 类型的对象。
type Selector interface{}
