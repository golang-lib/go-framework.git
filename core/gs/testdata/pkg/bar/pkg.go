package pkg

import (
	"fmt"
)

// golang 允许不同的路径下存在相同的包，而且允许存在相同的包。
type SamePkg struct{}

func (p *SamePkg) Package() {
	fmt.Println("github.com/go-spring/spring-core/gs/testdata/pkg/bar/pkg.SamePkg")
}
