package hash

import (
	"crypto/md5"
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// FileMD5 count file md5
func FileMD5(path string) (string, error) {
	file, err := os.Open(filepath.Clean(path))
	if err != nil {
		return "", err
	}
	defer file.Close()

	m := md5.New() // #nosec
	if _, err := io.Copy(m, file); err != nil {
		return "", err
	}

	fileMd5 := fmt.Sprintf("%x", m.Sum(nil))
	return fileMd5, nil
}

// FileSHA256 count file sha256
func FileSHA256(path string) (string, error) {
	file, err := os.Open(filepath.Clean(path))
	if err != nil {
		return "", err
	}
	defer file.Close()

	s := sha256.New()
	if _, err := io.Copy(s, file); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", s.Sum(nil)), nil
}
