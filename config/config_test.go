package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLoadLocalFile(t *testing.T) {
	var conf struct {
		ID    int    `json:"id,omitempty"`
		Name  string `json:"name,omitempty"`
		Phone string `json:"phone,omitempty"`
		City  string `json:"city,omitempty"`
	}
	e := LoadConfig(&conf)
	assert.NoError(t, e)
	assert.Equal(t, 1000, conf.ID)         // same in default and file
	assert.Equal(t, "yang jun", conf.Name) // empty in file, use default value
	assert.Equal(t, "1234", conf.Phone)    // empty default, use value in file
	assert.Equal(t, "Shanghai", conf.City) // override by file
}
