package starter

import (
	"gitee.com/golang-lib/go-framework/core/log"
	"reflect"
	"sort"
)

//服务启动注册器
type starterRegister struct {
	starters []Starter
}

var StarterRegister = &starterRegister{}

//注册starter
func Register(starter Starter) {
	StarterRegister.Register(starter)
}

//获取所有注册的starter
func GetStarters() []Starter {
	return StarterRegister.AllStarters()
}

func SortStarters() {
	sort.Sort(Starters(StarterRegister.AllStarters()))
}

//注册启动器
func (r *starterRegister) Register(starter Starter) {
	r.starters = append(r.starters, starter)
	SortStarters()
	typ := reflect.TypeOf(starter)
	log.Infof("Register starter: %s", typ.String())
}

//返回所有的启动器
func (r *starterRegister) AllStarters() []Starter {
	return r.starters
}
