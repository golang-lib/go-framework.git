package starter

import (
	"gitee.com/golang-lib/go-framework/core/log"
	"reflect"
)

type BootApplication struct {
	IsTest     bool
	starterCtx StarterContext
}

func (b *BootApplication) Start() {
	//1. 初始化starter
	b.init()
	//2. 安装starter
	b.setup()
	//3. 启动starter
	b.start()
}

//程序初始化
func (b *BootApplication) init() {
	log.Info("Initializing starters...")
	for _, v := range GetStarters() {
		typ := reflect.TypeOf(v)
		log.Debugf("Initializing: PriorityGroup=%d,Priority=%d,type=%s", v.PriorityGroup(), v.Priority(), typ.String())
		v.Init(b.starterCtx)
	}
}

//程序安装
func (b *BootApplication) setup() {
	log.Info("Setup starters...")
	for _, v := range GetStarters() {
		typ := reflect.TypeOf(v)
		log.Debug("Setup: ", typ.String())
		v.Setup(b.starterCtx)
	}
}

//程序开始运行，开始接受调用
func (b *BootApplication) start() {
	log.Info("Starting starters...")
	for _, v := range GetStarters() {
		typ := reflect.TypeOf(v)
		log.Debug("Starting: ", typ.String())
		v.Start(b.starterCtx)
	}
}

func (b *BootApplication) Stop() {
	log.Info("Stoping starters...")
	for _, v := range GetStarters() {
		typ := reflect.TypeOf(v)
		log.Debug("Stoping: ", typ.String())
		v.Stop(b.starterCtx)
	}
}
