package lru

import "sync"

type lruCacheItem struct {
	// 双向链表
	prev, next *lruCacheItem
	// 缓存数据
	data interface{}
	// 缓存数据对应的key
	key string
}

type lruCache struct {
	// 链表头指针和尾指针
	head, tail *lruCacheItem
	// 一个map存储各个链表的指针，以方便o(1)的复杂度读取数据
	lruMap map[string]*lruCacheItem
	rw     sync.RWMutex
	size   int64
}

func NewLRU(size int64) *lruCache {
	if size < 0 {
		size = 100
	}
	lru := &lruCache{
		head:   new(lruCacheItem),
		tail:   new(lruCacheItem),
		lruMap: make(map[string]*lruCacheItem),
		size:   size,
	}
	lru.head.next = lru.tail
	lru.tail.prev = lru.head
	return lru
}

func (lru *lruCache) Set(key string, v interface{}) {
	// fast path
	if _, exist := lru.lruMap[key]; exist {
		return
	}
	node := &lruCacheItem{
		data: v,
		prev: lru.head,
		next: lru.head.next,
		key:  key,
	}
	// add first
	lru.rw.Lock()
	defer lru.rw.Unlock()

	// double check
	if _, exist := lru.lruMap[key]; !exist {
		lru.lruMap[key] = node
		lru.head.next = node
		node.next.prev = node
	}

	if len(lru.lruMap) > int(lru.size) {
		// delete tail
		lastNode := lru.tail.prev
		lastNode.prev.next = lru.tail
		lru.tail.prev = lastNode.prev

		lastNode.next = nil
		lastNode.prev = nil
		delete(lru.lruMap, lastNode.key)
	}
}

func (lru *lruCache) Get(key string) (interface{}, bool) {
	lru.rw.RLock()
	v, ok := lru.lruMap[key]
	lru.rw.RUnlock()
	if ok {
		// move to head.next
		lru.rw.Lock()
		v.prev.next = v.next
		v.next.prev = v.prev

		v.prev = lru.head
		v.next = lru.head.next
		lru.head.next = v
		lru.rw.Unlock()
		return v.data, true
	}
	return nil, false
}
