package errors

import "fmt"

var (
	//基础相关
	ParamsError           = &CustomError{Code: 100002, Msg: "参数验证失败"}
	AssignError           = &CustomError{Code: 100003, Msg: "数据赋值失败"}
	DBError               = &CustomError{Code: 100004, Msg: "数据库操作失败"}
	DBNotFoundError       = &CustomError{Code: 100005, Msg: "未查询到指定数据"}
	UserNameNotFoundError = &CustomError{Code: 100006, Msg: "账号不存在"}
	UserNameDisableError  = &CustomError{Code: 100007, Msg: "账号已被禁用"}
	PasswordError         = &CustomError{Code: 100008, Msg: "账号密码错误"}
	RsaPasswordError      = &CustomError{Code: 100009, Msg: "非法账号密码"}
	IpLimitLoginError     = &CustomError{Code: 100010, Msg: "当前设备登陆错误次数过多,今日已被限制登陆"}
	SuperAdminEditError   = &CustomError{Code: 100011, Msg: "超级管理员不允许修改"}
	SuperAdminDelError    = &CustomError{Code: 100012, Msg: "超级管理员不允许删除"}

	//auth相关
	DulDeviceLoginError  = &CustomError{Code: 401, Msg: "你已在其他设备登陆"}
	NotResourcePower     = &CustomError{Code: 403, Msg: "暂无接口资源权限"}
	TokenValidateError   = &CustomError{Code: 401, Msg: "token验证失败"}
	TokenEmptyError      = &CustomError{Code: 401, Msg: "token信息不存在"}
	TokenExpiredError    = &CustomError{Code: 403, Msg: "登陆信息已过期，请重新登陆"}
	RefTokenExpiredError = &CustomError{Code: 401, Msg: "登陆信息已过期，请重新登陆"}

	//menu相关
	DulMenuNameError  = &CustomError{Code: 1000030, Msg: "菜单name值不能重复"}
	MenuParentIdError = &CustomError{Code: 1000031, Msg: "父菜单不能为自己"}

	//team相关
	NotAddTeamError      = &CustomError{Code: 1000040, Msg: "暂无此部门的下级部门创建权限"}
	NotEditTeamError     = &CustomError{Code: 1000041, Msg: "暂无此部门的修改权限"}
	NotDelTeamError      = &CustomError{Code: 1000042, Msg: "暂无此部门的删除权限"}
	NotAddTeamUserError  = &CustomError{Code: 1000043, Msg: "暂无此部门的人员创建权限"}
	NotEditTeamUserError = &CustomError{Code: 1000044, Msg: "暂无此部门的人员修改权限"}
	NotDelTeamUserError  = &CustomError{Code: 1000045, Msg: "暂无此部门的人员删除权限"}
	TeamParentIdError    = &CustomError{Code: 1000046, Msg: "父部门不能为自己"}

	//role相关
	DulKeywordError = &CustomError{Code: 1000050, Msg: "角色标志符已存在"}
)

const (
	defaultCode = 100001 //ums通用错误吗
)

var (
	/*New = func(msg string) error {
		return &CustomError{
			Code: defaultCode,
			Msg:  msg,
		}
	}*/

	NewF = func(msg string, arg ...interface{}) error {
		return &CustomError{
			Code: defaultCode,
			Msg:  fmt.Sprintf(msg, arg...),
		}
	}

	/*Is = func(err, tar error) bool {
		return errors.Is(err, tar)
	}*/
)

type CustomError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

func (c *CustomError) Error() string {
	return c.Msg
}
