package base

import (
	"encoding/json"
	"fmt"
)

const (
	SuccessCode = 0
	SuccessMsg  = "success"
)

type Result struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type PageInfo struct {
	Page  int `json:"page"`
	Size  int `json:"size"`
	Pages int `json:"pages"`
	Total int `json:"total"`
}

type PageResult struct {
	Result
	PageInfo PageInfo `json:"pageInfo"`
}

func (r *Result) IsSuccess() bool {
	return r.Code == SuccessCode
}

func (r *Result) ToJsonString() string {
	jsonData, err := json.Marshal(r)
	if err != nil {
		fmt.Println("data转json错误")
		return ""
	}
	return string(jsonData)
}

func Success(data interface{}) *Result {
	return &Result{Code: SuccessCode, Message: SuccessMsg, Data: data}
}

func SuccessNoData() *Result {
	return &Result{Code: SuccessCode, Message: SuccessMsg}
}

func ErrorBy(code int, msg string) *Result {
	return &Result{Code: code, Message: msg}
}

func BuildPageResult() *PageResult {

	return &PageResult{

	}
}
