package response

import (
	"gitee.com/golang-lib/go-framework/ginx"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"net/http"
)

type Result struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type PageInfo struct {
	Page  int `json:"page"`
	Size  int `json:"size"`
	Pages int `json:"pages"`
	Total int `json:"total"`
}

type PageResult struct {
	Result
	PageInfo PageInfo `json:"pageInfo"`
}

func BuildPageResult(data interface{}, page, size int, total int64) *PageResult {
	return &PageResult{
		Result:   Result{Data: data},
		PageInfo: PageInfo{Page: page, Size: size, Total: int(total)},
	}
}

func Ok(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{"code": 0, "message": "请求成功"})
}

func OkData(ctx *gin.Context, data interface{}) {
	ctx.JSON(http.StatusOK, gin.H{"code": 0, "data": data, "message": "请求成功"})
}

func OkPageData(ctx *gin.Context, data interface{}) {
	ctx.JSON(http.StatusOK, data)
}

func SuccessPage(ctx *gin.Context, page, size, count int, list interface{}) {
	result := Result{Code: 0, Message: "请求成功", Data: list}
	pages := CalculatePage(count, size)
	pageInfo := PageInfo{Page: page, Size: size, Pages: pages, Total: count}
	pageResult := PageResult{
		Result:   result,
		PageInfo: pageInfo,
	}
	ctx.JSON(http.StatusOK, pageResult)
}

func ClientError(ctx *gin.Context, err error) {
	if errs, ok := err.(validator.ValidationErrors); ok {
		ctx.JSON(http.StatusOK, gin.H{"code": 400, "message": ginx.GetValidationErrors(errs)})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 400, "message": err.Error()})
	}
}

func Fail(ctx *gin.Context, code int, message string) {
	ctx.JSON(http.StatusOK, gin.H{"code": code, "message": message})
}

func Error(ctx *gin.Context, err error) {
	ctx.JSON(http.StatusOK, gin.H{"code": 500, "message": err.Error()})
}

func (r *Result) IsSuccess() bool {
	return r.Code == 0
}

func CalculatePage(total, size int) int {
	if size == 0 {
		return 0
	}
	return (total + size - 1) / size
}
