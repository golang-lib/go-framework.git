package ginx

import (
	"github.com/gin-gonic/gin"
	"time"
)

type ReqCtx struct {
	GinCtx  *gin.Context
	cost    int64 // 执行时间
	TraceID string
}

type HandlerFunc func(*ReqCtx)

func NewReqCtxWithGin(g *gin.Context) *ReqCtx {
	return &ReqCtx{GinCtx: g}
}

func (rc *ReqCtx) Handle(handler HandlerFunc) {
	//ginCtx := rc.GinCtx
	//assert.IsTrue(ginCtx != nil, "ginContext == nil")

	defer func() {

	}()

	begin := time.Now()
	handler(rc)
	rc.cost = time.Since(begin).Milliseconds()
}

func (rc *ReqCtx) RespSuccess() {

}
