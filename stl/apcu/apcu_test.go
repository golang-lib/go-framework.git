package apcu_test

import (
	"encoding/json"
	"gitee.com/golang-lib/go-framework/stl/apcu"
	"gitee.com/golang-lib/go-framework/stl/assert"
	"testing"
)

func TestAPCU(t *testing.T) {

	type Resp struct {
		ErrNo  int    `json:"errno"`
		ErrMsg string `json:"errmsg"`
	}

	t.Run("", func(t *testing.T) {
		apcu.Store("success", `{"errno":200,"errmsg":"OK"}`)

		var resp *Resp
		load, err := apcu.Load("success", &resp)
		assert.Nil(t, err)
		assert.True(t, load)
		assert.Equal(t, resp, &Resp{200, "OK"})

		load, err = apcu.Load("success", &resp)
		assert.Nil(t, err)
		assert.True(t, load)
		assert.Equal(t, resp, &Resp{200, "OK"})
	})

	apcu.Delete("success")

	t.Run("", func(t *testing.T) {
		apcu.Store("success", `{"errno":200,"errmsg":"OK"}`)

		var resp map[string]interface{}
		load, err := apcu.Load("success", &resp)
		assert.Nil(t, err)
		assert.True(t, load)
		assert.Equal(t, resp, map[string]interface{}{
			"errno": json.Number("200"), "errmsg": "OK",
		})

		load, err = apcu.Load("success", &resp)
		assert.Nil(t, err)
		assert.True(t, load)
		assert.Equal(t, resp, map[string]interface{}{
			"errno": json.Number("200"), "errmsg": "OK",
		})
	})
}
