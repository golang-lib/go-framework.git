package knife_test

import (
	"context"
	"gitee.com/golang-lib/go-framework/stl/assert"
	"gitee.com/golang-lib/go-framework/stl/knife"
	"testing"
)

func TestKnife(t *testing.T) {
	ctx := context.TODO()

	v := knife.Get(ctx, "a")
	assert.Nil(t, v)

	knife.Set(ctx, "a", "b")

	v = knife.Get(ctx, "a")
	assert.Nil(t, v)

	ctx = knife.New(ctx)

	v = knife.Get(ctx, "a")
	assert.Nil(t, v)

	knife.Set(ctx, "a", "b")

	v = knife.Get(ctx, "a")
	assert.Equal(t, v, "b")
}
