package knife

import (
	"context"
	"sync"
)

const ctxKey = "::knife::"

func cache(ctx context.Context) *sync.Map {
	c, _ := ctx.Value(ctxKey).(*sync.Map)
	return c
}

// New 返回带有缓存空间的 context.Context 对象。
func New(ctx context.Context) context.Context {
	return context.WithValue(ctx, ctxKey, new(sync.Map))
}

// Get 从 context.Context 对象中获取 key 对应的 val。
func Get(ctx context.Context, key string) interface{} {
	if c := cache(ctx); c != nil {
		v, _ := c.Load(key)
		return v
	}
	return nil
}

// Set 将 key 及其 val 保存到 context.Context 对象。
func Set(ctx context.Context, key string, val interface{}) {
	if c := cache(ctx); c != nil {
		c.Store(key, val)
	}
}
