package util

// SafeCloseChan 安全地关闭一个通道。
func SafeCloseChan(ch chan struct{}) {
	select {
	case <-ch:
		// chan 已关闭，无需再次关闭。
	default:
		close(ch)
	}
}
