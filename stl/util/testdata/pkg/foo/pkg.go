package pkg

import (
	"fmt"
)

// SamePkg Golang 允许不同的路径下存在同名的包。
type SamePkg struct{}

func (p *SamePkg) Package() {
	fmt.Println("github.com/go-spring/spring-stl/util/testdata/pkg/foo/pkg.SamePkg")
}
