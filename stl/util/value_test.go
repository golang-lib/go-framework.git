package util_test

import (
	"fmt"
	"gitee.com/golang-lib/go-framework/stl/util"
	"testing"
)

func fnNoArgs() {}

func fnWithArgs(i int) {}

type receiver struct{}

func (r receiver) fnNoArgs() {}

func (r receiver) fnWithArgs(i int) {}

func (r *receiver) ptrFnNoArgs() {}

func (r *receiver) ptrFnWithArgs(i int) {}

func TestFileLine(t *testing.T) {

	fmt.Println(util.FileLine(fnNoArgs))
	//fmt.Println(util.FileLine(fnWithArgs))
	//fmt.Println(util.FileLine(receiver{}.fnNoArgs))
	//fmt.Println(util.FileLine(receiver{}.fnWithArgs))
	//fmt.Println(util.FileLine((&receiver{}).ptrFnNoArgs))
	//fmt.Println(util.FileLine((&receiver{}).ptrFnWithArgs))
	//fmt.Println(util.FileLine(receiver.fnNoArgs))
	//fmt.Println(util.FileLine(receiver.fnWithArgs))
	//fmt.Println(util.FileLine((*receiver).ptrFnNoArgs))
	//fmt.Println(util.FileLine((*receiver).ptrFnWithArgs))
	//
	//fmt.Println(util.FileLine(testdata.FnNoArgs))
	//fmt.Println(util.FileLine(testdata.FnWithArgs))
	//fmt.Println(util.FileLine(testdata.Receiver{}.FnNoArgs))
	//fmt.Println(util.FileLine(testdata.Receiver{}.FnWithArgs))
	//fmt.Println(util.FileLine((&testdata.Receiver{}).PtrFnNoArgs))
	//fmt.Println(util.FileLine((&testdata.Receiver{}).PtrFnWithArgs))
	//fmt.Println(util.FileLine(testdata.Receiver.FnNoArgs))
	//fmt.Println(util.FileLine(testdata.Receiver.FnWithArgs))
	//fmt.Println(util.FileLine((*testdata.Receiver).PtrFnNoArgs))
	//fmt.Println(util.FileLine((*testdata.Receiver).PtrFnWithArgs))
}
