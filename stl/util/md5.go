package util

import (
	"crypto/md5"
	"encoding/hex"
)

// MD5 获取 MD5 计算后的字符串。
func MD5(str string) string {
	hash := md5.Sum([]byte(str))
	return hex.EncodeToString(hash[:])
}
