package contain

import (
	"container/list"
)

// Ints 在一个 int 数组中进行查找，找不到返回 -1。
func Ints(arr []int, val int) int {
	for i := 0; i < len(arr); i++ {
		if arr[i] == val {
			return i
		}
	}
	return -1
}

// Strings 在一个 string 数组中进行查找，找不到返回 -1。
func Strings(arr []string, val string) int {
	for i := 0; i < len(arr); i++ {
		if arr[i] == val {
			return i
		}
	}
	return -1
}

// List 在列表中查询指定元素，存在则返回列表项指针，不存在返回 nil。
func List(l *list.List, v interface{}) *list.Element {
	for e := l.Front(); e != nil; e = e.Next() {
		if e.Value == v {
			return e
		}
	}
	return nil
}
