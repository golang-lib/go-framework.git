package flat

import (
	"fmt"
)

// Struct 将结构体的公开字段打平。
func Struct(v interface{}) map[string]interface{} {
	return make(map[string]interface{})
}

// Map 将嵌套形式的 map 打平，map 必须是 map[string]interface{} 类型。
func Map(m map[string]interface{}) map[string]interface{} {
	p := make(map[string]interface{})
	flatMap("", m, p)
	return p
}

func flatValue(key string, v interface{}, out map[string]interface{}) {
	switch value := v.(type) {
	case map[string]interface{}:
		flatMap(key+".", value, out)
	case []interface{}:
		flatSlice(key, value, out)
	default:
		out[key] = v
	}
}

func flatSlice(prefix string, arr []interface{}, out map[string]interface{}) {
	for i, v := range arr {
		key := fmt.Sprintf("%s[%d]", prefix, i)
		flatValue(key, v, out)
	}
}

func flatMap(prefix string, m map[string]interface{}, out map[string]interface{}) {
	for k, v := range m {
		flatValue(prefix+k, v, out)
	}
}
