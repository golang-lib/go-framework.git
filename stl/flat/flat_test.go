package flat_test

import (
	"gitee.com/golang-lib/go-framework/stl/assert"
	"gitee.com/golang-lib/go-framework/stl/flat"
	"testing"
)

func TestFlatMap(t *testing.T) {
	m := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": "d",
			},
			"e": []interface{}{
				"f", "g",
			},
			"h": "i",
		},
		"j": []interface{}{
			"k", "l",
		},
		"m": "n",
	}
	ret := flat.Map(m)
	expect := map[string]interface{}{
		"a.b.c":  "d",
		"a.e[0]": "f",
		"a.e[1]": "g",
		"a.h":    "i",
		"j[0]":   "k",
		"j[1]":   "l",
		"m":      "n",
	}
	assert.Equal(t, ret, expect)
}
