package utils

import (
	"archive/tar"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func Tar(source, target string) error {
	filename := filepath.Base(source)
	target = filepath.Join(target, fmt.Sprintf("%s.tar", filename))
	fmt.Println(filepath.Dir(target))
	if err := os.MkdirAll(filepath.Dir(target), 0755); nil != err {
		return err
	}
	/*tarFile, err := os.Create(target)
	if nil != err {
		return err
	}
	defer Close(tarFile)

	tarball := tar.NewWriter(tarFile)
	defer Close(tarball)

	info, err := os.Stat(source)
	if nil != err {
		return nil
	}

	var baseDir string
	if info.IsDir() {
		baseDir = filepath.Base(source)
	}

	return filepath.Walk(source, func(path string, info os.FileInfo, err error) error {
		if nil != err {
			return err
		}
		header, err := tar.FileInfoHeader(info, info.Name())
		if nil != err {
			return err
		}

		if baseDir != "" {
			header.Name = filepath.Join(baseDir, strings.TrimPrefix(path, source))
		}

		if err := tarball.WriteHeader(header); nil != err {
			return err
		}

		if info.IsDir() {
			return nil
		}

		file, err := os.Open(path)
		if nil != err {
			return err
		}
		defer Close(file)
		_, err = io.Copy(tarball, file)
		return err
	})*/

	return nil
}

func UnTar(tarball, target string) error {
	reader, err := os.Open(tarball)
	if nil != err {
		return err
	}
	defer Close(reader)
	tarReader := tar.NewReader(reader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if nil != err {
			return err
		}

		path := filepath.Join(target, header.Name)
		info := header.FileInfo()
		if info.IsDir() {
			if err = os.MkdirAll(path, info.Mode()); nil != err {
				return err
			}
			continue
		}

		file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, info.Mode())
		if nil != err {
			return err
		}
		_, err = io.Copy(file, tarReader)
		Close(file)

		if nil != err {
			return err
		}
	}
	return nil
}
