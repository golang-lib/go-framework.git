package utils

import (
	"fmt"
	"testing"
	"time"
)

type BoxInfo struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    Box    `json:"data,omitempty"`
}

type Box struct {
	BoxId      string     `json:"box_id,omitempty"`
	BoxName    string     `json:"box_name,omitempty"`
	Software   Software   `json:"software,omitempty"`
	Hardware   Hardware   `json:"hardware,omitempty"`
	IsDisabled bool       `json:"is_disabled" bson:"is_disabled"` //是否禁用
	Services   []Services `json:"services" bson:"services"`
	ReportedAt time.Time  `json:"reported_at" bson:"reported_at" validate:"required"`

	Extra   interface{} `json:"extra,omitempty" bson:"extra,omitempty"`     // 额外信息
	Cameras interface{} `json:"cameras,omitempty" bson:"cameras,omitempty"` // 车型识别里的摄像头
	Task    interface{} `json:"task,omitempty" bson:"task,omitempty"`       // 车型识别里的任务
}

type Software struct {
	Version string `json:"version"`
}

type Hardware struct {
	Version      string `json:"version"`       // 硬件版本
	Model        string `json:"model"`         // 设备型号
	SerialNumber string `json:"serial_number"` // 设备序列号
	DiskInfo     string `json:"disk_info" `    // 硬盘信息
	ChannelNum   int    `json:"channel_num"`   // 通道个数

	Temperature float64 `json:"temperature"` // 系统温度
	CpuRate     float64 `json:"cpu_rate"`    // CPU 使用率
	MemRate     float64 `json:"mem_rate"`    // 内存使用率
	DiskUsed    float64 `json:"disk_used"`   // 存储已使用
	DiskAvail   float64 `json:"disk_avail"`  // 存储未使用
}

type Services struct {
	ServiceId  string    `json:"service_id"`
	Version    string    `json:"version"`
	Status     int       `json:"status"`
	UpdateTime time.Time `json:"update_time" bson:"update_time"`
	SwitchedAt time.Time `json:"switched_at" bson:"switched_at"`
}

func TestGet(t *testing.T) {
	url := "http://100.100.69.130:10066/v1/box"

	var result BoxInfo
	err := Get(url, &result)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("result is: %+v\n", result)
	}
}
