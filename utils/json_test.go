package utils

import (
	"encoding/json"
	"testing"
)

func TestMarshalJSON(t *testing.T) {
	data, err := json.Marshal(map[string]string{"foo": "bar"})
	if nil != err {
		t.Fail()
	}
	if "{\"foo\":\"bar\"}" != (string(data)) {
		t.Fail()
	}
	if `{"foo":"bar"}` != (string(data)) {
		t.Fail()
	}
}

func TestUnMarshalJSON(t *testing.T) {
	var m map[string]interface{}
	err := json.Unmarshal([]byte(`{"foo":"bar"}`), &m)
	if nil != err {
		t.Fail()
	}
	if "bar" != m["foo"] {
		t.Fail()
	}
}
