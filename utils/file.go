package utils

import (
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// IsExist determines whether the file spcified by the given path is exists.
func IsExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil || os.IsExist(err)
}

// IsDir determines whether the specified path is a directory.
func IsDir(path string) bool {
	fio, err := os.Lstat(path)
	if os.IsNotExist(err) {
		return false
	}
	if nil != err {
		//logger.Warnf("determines whether [%s] is a directory failed: [%v]", path, err)
		return false
	}
	return fio.IsDir()
}

// IsBinary determines whether the specified content is a binary file content.
func IsBinary(content string) bool {
	for _, b := range content {
		if 0 == b {
			return true
		}
	}
	return false
}

// IsImg determines whether the specified extension is a image.
func IsImg(extension string) bool {
	ext := strings.ToLower(extension)
	switch ext {
	case ".jpg", ".jpeg", ".bmp", ".gif", ".png", ".svg", ".ico":
		return true
	default:
		return false
	}
}

// GetFileSize get the length in bytes of file of the specified path.
func GetFileSize(path string) int64 {
	fi, err := os.Stat(path)
	if nil != err {
		//logger.Error(err)
		return -1
	}
	return fi.Size()
}

// Copy copies the source to the dest.
// Keep the dest access/mod time as the same as the source.
func Copy(source, dest string) (err error) {
	if !IsExist(source) {
		return os.ErrNotExist
	}
	if IsDir(source) {
		return CopyDir(source, dest)
	}
	return CopyFile(source, dest)
}

// CopyDir copies the source directory to the dest directory.
// Keep the dest access/mod time as the same as the source.
func CopyDir(source, dest string) (err error) {
	return copyDir(source, dest, true)
}

func copyDir(source, dest string, chtimes bool) (err error) {
	sourceInfo, err := os.Stat(source)
	if err != nil {
		return err
	}

	if err = os.MkdirAll(dest, 0755); err != nil {
		return err
	}

	dirs, err := os.ReadDir(source)
	if err != nil {
		return err
	}

	for _, f := range dirs {
		srcFilePath := filepath.Join(source, f.Name())
		destFilePath := filepath.Join(dest, f.Name())
		if f.IsDir() {
			err = copyDir(srcFilePath, destFilePath, chtimes)
			if err != nil {
				//logger.Error(err)
				return
			}
		} else {
			err = copyFile(srcFilePath, destFilePath, chtimes)
			if err != nil {
				//logger.Error(err)
				return
			}
		}
	}

	if chtimes {
		if err = os.Chtimes(dest, sourceInfo.ModTime(), sourceInfo.ModTime()); nil != err {
			return
		}
	}
	return nil
}

// CopyFile copies the source file to the dest file.
// Keep the dest access/mod time as the same as the source.
func CopyFile(source, dest string) (err error) {
	return copyFile(source, dest, true)
}

func copyFile(source, dest string, chtimes bool) (err error) {
	if err = os.MkdirAll(filepath.Dir(dest), 0755); nil != err {
		return
	}

	destFile, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer Close(destFile)

	sourceInfo, err := os.Stat(source)
	if nil != err {
		return
	}

	if err = os.Chmod(dest, sourceInfo.Mode()); nil != err {
		return
	}

	sourceFile, err := os.Open(source)
	if err != nil {
		return err
	}
	defer Close(sourceFile)

	_, err = io.Copy(destFile, sourceFile)
	if nil != err {
		return
	}

	if chtimes {
		if err = os.Chtimes(dest, sourceInfo.ModTime(), sourceInfo.ModTime()); nil != err {
			return
		}
	}
	return
}

// CopyNewtimes copies the source to the dest.
// Do not keep the dest access/mod time as the same as the source.
func CopyNewtimes(source, dest string) (err error) {
	if !IsExist(source) {
		return os.ErrNotExist
	}
	if IsDir(source) {
		return CopyDirNewtimes(source, dest)
	}
	return CopyFileNewtimes(source, dest)
}

// CopyFileNewtimes copies the source file to the dest file.
// Do not keep the dest access/mod time as the same as the source.
func CopyFileNewtimes(source, dest string) (err error) {
	return copyFile(source, dest, false)
}

// CopyDirNewtimes copies the source directory to the dest directory.
// Do not keep the dest access/mod time as the same as the source.
func CopyDirNewtimes(source, dest string) (err error) {
	return copyDir(source, dest, false)
}

// WriteFileSaferByHandle writes the data to a temp file and writes the original file if everything else succeeds.
// Note: This function does not close the file handle after writing data.
func WriteFileSaferByHandle(handle *os.File, data []byte) error {
	writePath := handle.Name()
	dir, name := filepath.Split(writePath)

	tmp := filepath.Join(dir, name+Rand.String(7)+".tmp")
	f, err := os.OpenFile(tmp, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0600)
	if nil != err {
		return err
	}

	if _, err = f.Write(data); nil == err {
		err = f.Sync()
	}

	if closeErr := f.Close(); nil == err {
		err = closeErr
	}

	if nil == err {
		err = handle.Truncate(0)
	}

	if nil == err {
		_, err = handle.WriteAt(data, 0)
	}

	if nil == err {
		err = handle.Sync()
	}

	if nil == err {
		_ = os.Remove(f.Name())
	}
	return err
}

// WriteFileSafer writes the data to a temp file and atomically move if everything else succeeds.
func WriteFileSafer(writePath string, data []byte, perm os.FileMode) error {
	// credits: https://github.com/vitessio/vitess/blob/master/go/ioutil2/ioutil.go

	dir, name := filepath.Split(writePath)
	tmp := filepath.Join(dir, name+Rand.String(7)+".tmp")
	f, err := os.OpenFile(tmp, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0600)
	if nil != err {
		return err
	}

	if _, err = f.Write(data); nil == err {
		err = f.Sync()
	}

	if closeErr := f.Close(); nil == err {
		err = closeErr
	}

	if permErr := os.Chmod(f.Name(), perm); nil == err {
		err = permErr
	}

	if nil == err {
		var renamed bool
		for i := 0; i < 3; i++ {
			err = os.Rename(f.Name(), writePath) // Windows 上重命名是非原子的
			if nil == err {
				renamed = true
				break
			}
			if errMsg := strings.ToLower(err.Error()); strings.Contains(errMsg, "access is denied") || strings.Contains(errMsg, "used by another process") { // 文件可能是被锁定
				time.Sleep(100 * time.Millisecond)
				continue
			}
			break
		}

		if !renamed {
			// 直接写入
			err = os.WriteFile(writePath, data, perm)
		}
	}

	if nil != err {
		_ = os.Remove(f.Name())
	}
	return err
}
