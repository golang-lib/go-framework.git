package utils

import (
	"errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

// 从指定路径加载yaml文件
func LoadYml(path string, out interface{}) error {
	yamlFileBytes, readErr := ioutil.ReadFile(path)
	if readErr != nil {
		return readErr
	}
	// yaml解析
	err := yaml.Unmarshal(yamlFileBytes, out)
	if err != nil {
		return errors.New("无法解析 [" + path + "] -- " + err.Error())
	}
	return nil
}

// yaml解析
func LoadYmlByString(yamlStr string, out interface{}) error {
	return yaml.Unmarshal([]byte(yamlStr), out)
}
