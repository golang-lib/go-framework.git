package utils

import "testing"

func TestToBytes(t *testing.T) {
	str := "Gulu 你好！"
	bytes := ToBytes(str)

	t.Log(bytes)
	t.Log([]byte(str))

	if str2 := FromBytes(bytes); str != str2 {
		t.Errorf("Str Bytes convert failed [str=%s, str2=%s]", str, str2)
	}
}

func TestContains(t *testing.T) {
	if !Contains([]string{"123", "345"}, "123") {
		t.Error("[\"123\", \"345\"] should contain \"123\"")
		return
	}
}

func TestReplaceIgnoreCase(t *testing.T) {
	expected := "Foabcdr"
	got := ReplaceIgnoreCase("Foobar", "oBa", "abcd")
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}
}

func TestReplacesIgnoreCase(t *testing.T) {
	expected := "abcdbarefgh"
	got := ReplacesIgnoreCase("Foobarbaz", "foo", "abcd", "baz", "efgh")
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}

	expected = "bar baz baz"
	got = ReplacesIgnoreCase("foo bar baz", "foo", "bar", "bar", "baz")
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}

	expected = "bar baz baz"
	got = ReplacesIgnoreCase("foo bar baz", "Bar", "baz", "foo", "bar")
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}

	expected = "fazz bar barr"
	got = ReplacesIgnoreCase("foo bar baz", "oo", "azz", "az", "arr")
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}
}

func TestEncloseIgnoreCase(t *testing.T) {
	var expected, got string
	expected = "<mark>Foo</mark>bar<mark>baz</mark>"
	got = EncloseIgnoreCase("Foobarbaz", "<mark>", "</mark>", "foo", "baz")
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}
	expected = "F<mark>oo</mark><mark>ba</mark>r<mark>ba</mark>z"
	got = EncloseIgnoreCase("Foobarbaz", "<mark>", "</mark>", "Oo", "Ba")
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}
}

func TestLCS(t *testing.T) {
	str := LCS("123456", "abc34def")
	if "34" != str {
		t.Error("[\"123456\"] and [\"abc34def\"] should have the longest common substring [\"34\"]")
		return
	}
}

func TestSubStr(t *testing.T) {
	expected := "foo测"
	got := SubStr("foo测试bar", 4)
	if expected != got {
		t.Errorf("expected [%s], got [%s]", expected, got)
		return
	}
}
