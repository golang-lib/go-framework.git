package utils

import (
	"os"
	"testing"
)

var testdataDir = "testdata"
var zipDirPath = "/Users/yangjun/Desktop/test"
var zipFi = "/Users/yangjun/Desktop/test/test.zip"

func TestCreate(t *testing.T) {
	zipFile, err := Create(zipFi)
	if nil != err {
		t.Error(err)
		return
	}
	err = zipFile.AddDirectoryN(".", testdataDir)
	if nil != err {
		t.Error(err)
		return
	}
	err = zipFile.Close()
	if nil != err {
		t.Error(err)
		return
	}
}

func TestUnzip(t *testing.T) {
	err := Unzip(zipFi, zipDirPath)
	if nil != err {
		t.Error(err)
		return
	}
}

func TestMain(m *testing.M) {
	retCode := m.Run()

	// clean test data
	os.RemoveAll(zipDirPath + ".zip")
	os.RemoveAll(zipDirPath)

	os.RemoveAll(tarDirPath + ".tar")
	os.RemoveAll(tarDirPath)
	//os.RemoveAll(untarDirPath)

	os.Exit(retCode)
}

func _TestEmptyDir(t *testing.T) {
	dir1 := "/dir/subDir1"
	dir2 := "/dir/subDir2"

	err := os.MkdirAll(zipDirPath+dir1, os.ModeDir)
	if nil != err {
		t.Error(err)
		return
	}

	err = os.MkdirAll(zipDirPath+dir2, os.ModeDir)
	if nil != err {
		t.Error(err)
		return
	}

	f, err := os.Create(zipDirPath + dir2 + "/file")
	if nil != err {
		t.Error(err)
		return
	}
	f.Close()

	zipFile, err := Create(zipDirPath + "/dir.zip")
	if nil != err {
		t.Error(err)

		return
	}

	zipFile.AddDirectoryN("dir", zipDirPath+"/dir")
	if nil != err {
		t.Error(err)
		return
	}

	err = zipFile.Close()
	if nil != err {
		t.Error(err)
		return
	}

	err = Unzip(zipDirPath+"/dir.zip", zipDirPath+"/unzipDir")
	if nil != err {
		t.Error(err)
		return
	}

	if !IsExist(zipDirPath+"/unzipDir") || !IsDir(zipDirPath+"/unzipDir") {
		t.Error("Unzip failed")
		return
	}

	if !IsExist(zipDirPath+"/unzipDir"+dir1) || !IsDir(zipDirPath+"/unzipDir"+dir1) {
		t.Error("Unzip failed")
		return
	}

	if !IsExist(zipDirPath+"/unzipDir"+dir2) || !IsDir(zipDirPath+"/unzipDir"+dir2) {
		t.Error("Unzip failed")
		return
	}

	if !IsExist(zipDirPath+"/unzipDir"+dir2+"/file") || IsDir(zipDirPath+"/unzipDir"+dir2+"/file") {
		t.Error("Unzip failed")
		return
	}
}
