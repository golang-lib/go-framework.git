package utils

import (
	"fmt"
	"gitee.com/golang-lib/go-framework/core/http"
	"log"
	"net"
	"sync"
)

const UNKNOWN = "XX XX"
const ipUrl = "http://whois.pconline.com.cn/ipJson.jsp"

//获取真实地址
func GetRealAddressByIP(ip string) string {
	if ip == "127.0.0.1" || ip == "localhost" {
		return "内部IP"
	}
	url := fmt.Sprintf("%s?ip=%s&json=true", ipUrl, ip)
	res := http.NewRequest(url).Get()
	if res == nil || res.StatusCode != 200 {
		return UNKNOWN
	}
	resultMap, err := res.BodyToMap()
	if err != nil {
		return UNKNOWN
	}
	log.Println(fmt.Sprintf("%s %s", resultMap["pro"].(string), resultMap["city"].(string)))
	return resultMap["addr"].(string)
}

// 获取局域网ip地址
func GetLocalHost() string {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		fmt.Println("net.Interfaces failed, err:", err.Error())
	}

	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addrs, _ := netInterfaces[i].Addrs()
			for _, address := range addrs {
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
					if ipnet.IP.To4() != nil {
						return ipnet.IP.String()
					}
				}
			}
		}

	}
	return ""
}

var localIPv4Str = "0.0.0.0"
var localIPv4Once = new(sync.Once)

// LocalIPv4 获取本机的 IPv4 地址。
func LocalIPv4() string {
	localIPv4Once.Do(func() {
		if ias, err := net.InterfaceAddrs(); err == nil {
			for _, address := range ias {
				if ipNet, ok := address.(*net.IPNet); ok && !ipNet.IP.IsLoopback() {
					if ipNet.IP.To4() != nil {
						localIPv4Str = ipNet.IP.String()
						return
					}
				}
			}
		}
	})
	return localIPv4Str
}
