package utils

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

func Get(url string, result interface{}) error {
	return _doRequest("GET", url, nil, nil, result)
}

func _doRequest(method, url string, body io.Reader, header map[string]string, result interface{}, timeout ...time.Duration) error {
	client := http.DefaultClient

	if len(timeout) > 0 {
		client = &http.Client{Timeout: timeout[0], Transport: client.Transport}
	}

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return err
	}

	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	if header != nil {
		for k, s := range header {
			req.Header.Set(k, s)
		}
	}

	response, err := client.Do(req)
	if err != nil {
		return err
	}

	defer Close(response.Body)

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, result)
	if err != nil {
		return err
	}

	return nil
}
