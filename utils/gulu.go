package utils

import (
	"os"
)

// Logger is the logger used in Gulu internally.
var logger = LogMgr.NewLogger(os.Stdout)

type (
	GuluRand struct{}
	GuluFile struct{}
	GuluJson struct{}
)

var (
	Rand GuluRand
	File GuluFile
	Json GuluJson
)
