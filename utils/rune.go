package utils

import "unicode"

// IsNumOrLetter checks the specified rune is number or letter.
func IsNumOrLetter(r rune) bool {
	return ('0' <= r && '9' >= r) || IsLetter(r)
}

// IsLetter checks the specified rune is letter.
func IsLetter(r rune) bool {
	return 'a' <= r && 'z' >= r || 'A' <= r && 'Z' >= r
}

// ContainChinese checks the specified string whether contains chinese.
func ContainChinese(str string) bool {
	var count int
	for _, v := range str {
		if unicode.Is(unicode.Han, v) {
			count++
			break
		}
	}
	return count > 0
}
