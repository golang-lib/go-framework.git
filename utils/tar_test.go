package utils

import (
	"os"
	"path/filepath"
	"testing"
)

var tarDirPath = "/Users/yangjun/Desktop/test/aa/"
var unTarDirPath = "testdata_untar"

func TestMakeDir(t *testing.T) {
	var tarDirPath2 = "/Users/yangjun/Desktop/test/aa/bb"
	err := os.MkdirAll(tarDirPath2, os.ModePerm)
	if err != nil {
		t.Error(err)
	}
}

func TestTar(t *testing.T) {
	if !IsExist(tarDirPath) {
		err := os.MkdirAll(tarDirPath, os.ModePerm)
		if err != nil {
			t.Error(err)
		}
	}

	/*if err := os.MkdirAll(filepath.Dir(tarDirPath), 0755); nil != err {
		t.Error(err)
	}*/

	/*	if err := Tar("testdata/README.md", tarDirPath); nil != err {
		t.Error(err)
		return
	}*/
}

func TestUnTar(t *testing.T) {
	err := UnTar(filepath.Join(tarDirPath, testdataDir+".tar"), unTarDirPath)
	if nil != err {
		t.Error(err)
		return
	}
}
