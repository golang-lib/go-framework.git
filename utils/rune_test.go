package utils

import "testing"

func TestIsNumOrLetter(t *testing.T) {
	if !IsNumOrLetter('0') {
		t.Fail()
	}
	if IsNumOrLetter('@') {
		t.Fail()
	}
}

func TestIsLetter(t *testing.T) {
	if !IsLetter('a') {
		t.Fail()
	}
	if IsLetter('0') {
		t.Fail()
	}
}

func TestContainsChinese(t *testing.T) {
	if !ContainChinese("Hello 世界") {
		t.Fail()
	}
	if ContainChinese("Hello World") {
		t.Fail()
	}
	if ContainChinese("Hello，World") {
		t.Fail()
	}
}
