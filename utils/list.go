package utils

import (
	"container/list"
)

// NewList 使用输入的元素创建列表。
func NewList(v ...interface{}) *list.List {
	l := list.New()
	for _, val := range v {
		l.PushBack(val)
	}
	return l
}
