package utils

import "testing"

func TestLocalIP(t *testing.T) {
	ip, err := LocalIP()
	if nil != err {
		t.Error(err)
	}
	t.Log(ip)
}

func TestLocalMac(t *testing.T) {
	mac, err := LocalMac()
	if nil != err {
		t.Error(err)
	}
	t.Log(mac)
}
