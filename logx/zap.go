package logx

import (
	"context"
	"time"
)

type zapAdapt struct {
}

func NewZapAdapt(cfg *LogConfig) ILogger {
	adapt := &zapAdapt{
	}
	return adapt
}

func (s zapAdapt) WithCallerSkip(skip int) ILogger {
	panic("implement me")
}

func (s zapAdapt) WithFields(fields ...LogField) ILogger {
	panic("implement me")
}

func (s zapAdapt) WithContext(ctx context.Context) ILogger {
	panic("implement me")
}

func (s zapAdapt) WithDuration(d time.Duration) ILogger {
	panic("implement me")
}

func (s zapAdapt) Trace(args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Debug(args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Info(args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Warn(args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Error(args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Panic(args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Fatal(args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Tracef(format string, args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Debugf(format string, args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Infof(format string, args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Warnf(format string, args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Errorf(format string, args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Panicf(format string, args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) Fatalf(format string, args ...interface{}) {
	panic("implement me")
}

func (s zapAdapt) SetOutputLevel(level string) {
	panic("implement me")
}
