package ginx

import (
	"gitee.com/golang-lib/go-framework/core/biz"
	"github.com/gin-gonic/gin"
	"strconv"
)

// 获取查询参数中指定参数值，并转为int
func QueryInt(c *gin.Context, key string, defaultVal int) int {
	val := c.Query(key)
	if val == "" {
		return defaultVal
	}
	intVal, err := strconv.Atoi(val)
	biz.AssertErrIsNil(err, "query param is not number")
	return intVal
}

// 绑定并校验请求结构体参数  结构体添加 例如： binding:"required" 或binding:"required,gt=10"
func BindJsonAndValid(g *gin.Context, data interface{}) {
	if err := g.ShouldBindJSON(data); err != nil {
		panic(biz.NewBizErr("传参格式错误：" + err.Error()))
	}
}
