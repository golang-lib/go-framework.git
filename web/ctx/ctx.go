package ctx

import "github.com/gin-gonic/gin"

type ReqCtx struct {
	GinCtx             *gin.Context // gin context
	NeedToken          bool         // 是否需要token
	LoginClaims        *Claims      // 登录账号信息，只有校验token后才会有值
	RequiredPermission *Permission  // 需要的权限信息，默认为nil
	LogInfo            *LogInfo     // 日志相关信息
	ReqParam           interface{}  // 请求参数，主要用于记录日志
	ResData            interface{}  // 响应结果
	Err                interface{}  // 请求错误
	timed              int64        // 执行时间
	noRes              bool         // 无需返回结果，即文件下载等
}

type HandlerFunc func(*ReqCtx)

func NewReqCtx() *ReqCtx {
	return &ReqCtx{}
}

func NewReqCtxWithGin(g *gin.Context) *ReqCtx {
	return &ReqCtx{GinCtx: g, RequiredPermission: &Permission{NeedToken: true, NeedCasBin: true}}
}
