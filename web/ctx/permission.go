package ctx

import "gitee.com/golang-lib/go-framework/core/biz"

type Permission struct {
	NeedToken  bool // 是否需要token
	NeedCasBin bool // 是否进行权限  api路径权限验证
}

func (p *Permission) WithNeedToken(needToken bool) *Permission {
	p.NeedToken = needToken
	return p
}

func (p *Permission) WithNeedCasBin(needCasBin bool) *Permission {
	p.NeedCasBin = needCasBin
	return p
}

func PermissionHandler(rc *ReqCtx) error {
	permission := rc.RequiredPermission
	// 如果需要的权限信息不为空，并且不需要token，则不返回错误，继续后续逻辑
	if permission != nil && !permission.NeedToken {
		return nil
	}
	tokenStr := rc.GinCtx.Request.Header.Get("X-TOKEN")
	// header不存在则从查询参数token中获取
	if tokenStr == "" {
		tokenStr = rc.GinCtx.Query("token")
	}
	if tokenStr == "" {
		return biz.PermissionErr
	}
	loginAccount, err := ParseToken(tokenStr)
	if err != nil || loginAccount == nil {
		return biz.PermissionErr
	}
	rc.LoginClaims = loginAccount

	if !permission.NeedCasBin {
		return nil
	}

	// 判断策略中是否存在
	//e := casbin.Casbin()
	//success, _ := e.Enforce(loginAccount.RoleKey, rc.GinCtx.Request.URL.Path, rc.GinCtx.Request.Method)
	//if !success {
	//	return biz.CasbinErr
	//}

	return nil
}
