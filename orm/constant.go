package orm

const (
	And     = "AND"
	Or      = "OR"
	In      = "IN"
	Not     = "NOT"
	Like    = "LIKE"
	Eq      = "="
	Ne      = "<>"
	Gt      = ">"
	Ge      = ">="
	Lt      = "<"
	Le      = "<="
	Between = "BETWEEN"
	Desc    = "DESC"
	Asc     = "ASC"
)
