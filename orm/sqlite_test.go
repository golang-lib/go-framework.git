package orm

import (
	"fmt"
	"gorm.io/gorm"
	"log"
	"reflect"
	"testing"
)

var orm IOrm

func init() {
	orm = NewGlobalOrm(&DbConfig{
		Type:       "sqlite",
		SqliteFile: "./test.db",
	})
	if err := orm.AutoMigrate(&User{}, &Role{}); err != nil {
		log.Fatalln("err:", err.Error())
	}
}

func TestInsertRole(t *testing.T) {
	role := &Role{
		Name: "ADMIN",
	}

	err := orm.Insert(role)
	if err != nil {
		log.Fatalln("err:", err.Error())
	}
}

func TestInsert(t *testing.T) {
	user := &User{
		Name:              "yj",
		Age:               18,
		EnableHeadCapture: true,
	}
	err := orm.Insert(user)
	if err != nil {
		log.Fatalln("err:", err.Error())
	}
}

func TestSelectById(t *testing.T) {
	var user User
	err := orm.SelectById(2, &user)
	if err != nil {
		log.Fatalln("err:", err.Error())
		return
	}
	log.Printf("%+v", user)
}

func TestSelectList(t *testing.T) {
	var list []*User
	query := NewQuery().WithTableName("user").Eq("age", 18).In("id", []int{1, 2, 3})
	err := orm.SelectList(query, &list)
	if err != nil {
		log.Fatalln("err:", err.Error())
		return
	}
	fmt.Println(list)
	for _, user := range list {
		fmt.Println(user.Name)
	}
}

func TestSelect(t *testing.T) {
	condition := &User{
		Model: gorm.Model{
			ID: 2,
		},
		Age:  18,
		Name: "yj",
	}

	var users []UserDto
	err := orm.Select(condition, &users)
	if err != nil {
		log.Fatalln("err:", err.Error())
		return
	}
	fmt.Println(users)
}

func TestSelectCount(t *testing.T) {
	query := NewQuery().Table(&User{}).Eq("age", 18).In("id", []int{1, 2, 3})
	count, err := orm.SelectCount(query)
	if err != nil {
		log.Fatalln("err:", err.Error())
		return
	}
	fmt.Println(count)
}

func TestSelectPage(t *testing.T) {
	var users []User
	query := NewQuery().WithTableName("user").Eq("age", 18).In("id", []int{1, 2, 3})
	count, err := orm.SelectPage(query, &users)
	if err != nil {
		log.Fatalln("err:", err.Error())
		return
	}
	fmt.Println(count)
	fmt.Println(users)
}

func TestDeleteById(t *testing.T) {
	err := orm.DeleteById(&User{}, 1)
	if err != nil {
		log.Fatalln("err:", err.Error())
		return
	}
}

func TestUpdateById(t *testing.T) {
	user := &User{
		Name: "yj",
		Age:  20,
		Model: gorm.Model{
			ID: 1,
		},
	}

	returnValue := reflect.ValueOf(user)
	fmt.Println(returnValue.Kind())

	tt := []reflect.Value{returnValue}
	fmt.Println(tt)

	//err := orm.UpdateColumnById(user, "name", "yang jun")
	//if err != nil {
	//	log.Fatalln("err:", err.Error())
	//}
}

func TestReflect2(t *testing.T) {
	var users []User
	typ := reflect.TypeOf(users)
	fmt.Println(users)
	fmt.Println(typ)

	user := &User{}
	typ2 := reflect.TypeOf(user)
	t2 := reflect.SliceOf(typ2.Elem())
	destValueCanAddr := reflect.New(t2).Elem().Interface()
	fmt.Println(destValueCanAddr)
	fmt.Println(reflect.TypeOf(destValueCanAddr))
}

func TestReflect(t *testing.T) {
	user := User{
		Name: "yj",
		Age:  20,
	}

	user.Model = gorm.Model{
		ID: 10,
	}

	rdata := reflect.ValueOf(user)
	switch rdata.Kind() {
	case reflect.Map:
		fmt.Println("is a map")
	case reflect.Ptr:
		fmt.Println("is a ptr")
		fmt.Println(rdata.Elem().FieldByName("ID"))
		fmt.Println(rdata.Type())
		ptrValue := reflect.SliceOf(rdata.Type())
		fmt.Println(ptrValue.Kind())
		ptrValue2 := reflect.ArrayOf(0, rdata.Type())
		fmt.Println(ptrValue2.Kind())
	case reflect.Struct:
		fmt.Println("is a Struct")
		fmt.Println(rdata.FieldByName("Age").Interface())
		fmt.Println(rdata.Type())
		//ptrValue := reflect.SliceOf(rdata.Type())
		//fmt.Println(ptrValue.Kind())
	default:
		fmt.Println(rdata.FieldByName("Age").Interface())
	}

	//tp := reflect.TypeOf(user)
	//fmt.Printf("type_name:%+v kind:%+v \n", tp.Name(), tp.Kind())

	//for i := 0; i < tp.NumField(); i++ {
	//	field := tp.Field(i)
	//	fmt.Printf("field:%+v \n", field)
	//}

	//value := reflect.ValueOf(*user)
	//value := reflect.ValueOf(user)
	//fmt.Printf("value_name:%+v kind:%+v \n", value.Type(), value.Kind())

	//v := value.FieldByName("age")
	//fmt.Printf("v:%+v \n", v)

	//for i := 0; i < value.NumField(); i++ {
	//	field := value.Field(i)
	//	fmt.Printf("field:%+v \n", field)
	//}

	//for i := 0; i < value.NumField(); i++ {
	//	fmt.Println(value.FieldByName("age"))
	//}
}
