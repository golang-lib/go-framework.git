package orm

import (
	"gorm.io/gorm/schema"
	"log"
	"strings"
)

var (
	std IOrm
)

type IOrm interface {
	AutoMigrate(dst ...interface{}) error
	Insert(value interface{}) error
	SelectById(id uint, data interface{}) error
	SelectList(query *Query, data interface{}) error
	Select(table schema.Tabler, data interface{}) error
	SelectPage(query *Query, data interface{}) (count int64, err error)
	SelectCount(query *Query) (count int64, err error)
	DeleteById(table schema.Tabler, id uint) error
	UpdateColumnById(table schema.Tabler, column string, value interface{}) error
}

type DbConfig struct {
	Type       string
	SqliteFile string
}

func NewGlobalOrm(cfg *DbConfig) IOrm {
	if cfg.Type == "" {
		log.Fatalln("db type is needed!")
	}
	switch strings.ToLower(cfg.Type) {
	case "sqlite":
		std = NewSqliteAdapt(cfg)
	case "mysql":
		//std = NewMysqlAdapt(cfg)
	case "pg":
		//std = NewPgAdapt(cfg)
	default:
		log.Fatalln("init orm error")
	}
	return std
}
