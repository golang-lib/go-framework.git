package orm

import (
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model
	Name              string `gorm:"name"`
	Age               int    `gorm:"age" json:"age"`
	Mobile            string `gorm:"mobile"`
	EnableHeadCapture bool   `gorm:"column:enable_head_capture"`
}

func (c User) TableName() string {
	return "user"
}

type UserDto struct {
	Name   string `gorm:"name" json:"name"`
	Age    int    `gorm:"age" json:"age"`
	Mobile string `gorm:"mobile" json:"mobile"`
}

type Role struct {
	ID        uint   `gorm:"primarykey"`
	Name      string `gorm:"name"`
	SleepAt   *time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (c Role) TableName() string {
	return "role"
}
