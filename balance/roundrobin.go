package balance

import (
	"errors"
)

func init() {
	RegisterBalancer("roundrobin", &RoundRobinBalance{})
}

type RoundRobinBalance struct {
	curIndex int
}

func (p *RoundRobinBalance) DoBalance(instances []*Instance, key ...string) (inst *Instance, err error) {
	if len(instances) == 0 {
		err = errors.New("No instance")
		return
	}

	lens := len(instances)
	if p.curIndex >= lens {
		p.curIndex = 0
	}

	inst = instances[p.curIndex]
	p.curIndex = (p.curIndex + 1) % lens
	return
}
