package balance

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestBalance(t *testing.T) {
	var insts []*Instance
	for i := 0; i < 16; i++ {
		host := fmt.Sprintf("192.168.%d.%d", rand.Intn(255), rand.Intn(255))
		one := NewInstance(host, 8080)
		insts = append(insts, one)
	}

	var balanceName = "random"

	for {
		inst, err := DoBalance(balanceName, insts)
		if err != nil {
			fmt.Println("do balance err:", err)
			//fmt.Fprintf(os.Stdout, "do balance err\n")
			continue
		}
		fmt.Println(inst)
		time.Sleep(time.Second * 5)
	}
}
