package balance

import (
	"errors"
	"math/rand"
)

func init() {
	RegisterBalancer("random", &RandomBalance{})
}

type RandomBalance struct {
}

func (p *RandomBalance) DoBalance(instances []*Instance, key ...string) (inst *Instance, err error) {
	if len(instances) == 0 {
		err = errors.New("No instance")
		return
	}

	lens := len(instances)
	index := rand.Intn(lens)
	inst = instances[index]

	return
}
