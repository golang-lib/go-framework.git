package balance

import "fmt"

type IBalanceMgr struct {
	allBalancer map[string]Balancer
}

var mgr = IBalanceMgr{
	allBalancer: make(map[string]Balancer),
}

func (p *IBalanceMgr) registerBalancer(name string, b Balancer) {
	p.allBalancer[name] = b
}

func RegisterBalancer(name string, b Balancer) {
	mgr.registerBalancer(name, b)
}

func DoBalance(name string, instances []*Instance) (inst *Instance, err error) {
	balancer, ok := mgr.allBalancer[name]
	if !ok {
		err = fmt.Errorf("Not found %s balancer", name)
		return
	}
	fmt.Printf("use %s balancer: ", name)
	inst, err = balancer.DoBalance(instances)
	return
}
